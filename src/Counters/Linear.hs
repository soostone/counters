{-# LANGUAGE RecordWildCards #-}

{-| This module is not yet completed and is unusable in its current state -}

module Counters.Linear
    ( LinCounter
    , cardinality
    ) where


import           Control.Monad.IO.Class
import qualified Data.ByteString             as B
import           Data.Digest.SHA1            as Sha
import qualified Data.Vector.Unboxed         as V
import qualified Data.Vector.Unboxed.Mutable as MV
import           System.Environment



data LinCounter = LinCounter {
      bitmap :: V.Vector Bool
    , csize  :: Int
    }


cardinality :: Floating a => LinCounter -> a
cardinality LinCounter{..} = (-1) * fromIntegral csize * log load
    where
      load = 1 - (fromIntegral weight / fromIntegral csize)
      weight = V.foldl' step 0 bitmap
      step n b = if b then n + 1 else n



-- | make an update function from this test code
-- count :: Int -> FilePath -> IO LinCounter
-- count m fp = runResourceT $ do
--     v <- liftIO $ MV.new m
--     liftIO $ MV.set v False
--     sourceFile fp $=
--       intoCSV defCSVSettings $$
--       C.mapM_ (step v)
--     mx <- liftIO $ V.freeze v
--     return $ LinCounter mx m
--     where
--       step v [x] = do
--         -- liftIO $ putStrLn $ "Hash index: " ++ show k
--         liftIO $ MV.write v k True
--           where
--             h = fromIntegral . Sha.toInteger $ hash (B.unpack x)
--             k = h `mod` m



