{-# LANGUAGE RankNTypes          #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Counters.HyperLogLog.List where


import           Data.TypeLevel.Num.Sets
import qualified Data.Vector.Unboxed           as V
import           GHC.Int
-------------------------------------------------------------------------------
import           Counters.HyperLogLog.Internal




hllToList :: HLL n -> [Rank]
hllToList HLL{..} = V.toList buckets


-- | Parse HLL from a list of numbers representing bucket ranks.
hllFromList :: forall n. Nat n => [Int8] -> Either String (HLL n)
hllFromList l =
    case hllNumBuckets new == V.length v of
      True -> Right $ new { buckets = v }
      False -> Left $ "Loaded HLL has length " ++
                      show (V.length v) ++
                      " but this parse operation was expecting " ++
                      show (hllNumBuckets new) ++
                      " buckets."
    where
      v = V.fromList l

      new :: Nat n => HLL n
      new = initHLL


