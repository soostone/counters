{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}

module Counters.HyperLogLog.JSON where


import           Data.TypeLevel.Num.Sets
import           Data.Aeson
import qualified Data.Vector.Unboxed           as V
-------------------------------------------------------------------------------
import           Counters.HyperLogLog.Internal
import           Counters.HyperLogLog.List




instance ToJSON (HLL n) where
    toJSON HLL{..} = object
      [ "b" .= V.toList buckets ]


instance Nat n => FromJSON (HLL n) where
    parseJSON (Object v) = do
      arr <- v .: "b"
      either fail return $ hllFromList arr




