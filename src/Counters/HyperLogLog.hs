{-# LANGUAGE DeriveGeneric             #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE RecordWildCards           #-}
{-# LANGUAGE ScopedTypeVariables       #-}

module Counters.HyperLogLog
    ( HLL
    , hllBits
    , hllNumBuckets
    , castHLL

    , updateHLL
    , cardinality

    -- * Initializing counters of different sizes
    , initHLL
    , initHLL7, initHLL8, initHLL9, initHLL10
    , initHLL11, initHLL12, initHLL13
    , initHLL14, initHLL15, initHLL16

    -- * Type-level size annotations
    , Nat
    , D7, D8, D9, D10, D11, D12
    , D13, D14, D15, D16

    ) where


import           Counters.HyperLogLog.Internal
import           Data.TypeLevel.Num.Aliases
import           Data.TypeLevel.Num.Sets
import           Data.TypeLevel.Num.Reps
