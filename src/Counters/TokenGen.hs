module Main where

import           Control.Monad
import qualified Data.ByteString.Char8 as B
import           Data.RNG
import           System.Environment
import           System.IO


main = do
    i : fp : _ <- getArgs
    genTokens (read i) fp



-- | Generate i tokens into file fp. good for testing
genTokens i fp = do
    h <- openFile fp WriteMode
    rng <- mkRNG
    replicateM_ i $ do
      t <- randomToken 64 rng
      B.hPutStrLn h t
      hSetBuffering h LineBuffering


