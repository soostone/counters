{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}

module Main where

import           Control.Lens
import           Data.Analytics.Approximate.HyperLogLog
import           Data.Analytics.Approximate
import           Data.Analytics.Reflection
import           Data.Monoid

import           Data.ByteString.Char8 (ByteString)
import           Data.CSV.Conduit
import           Data.Conduit
import           Data.Conduit.Binary   (sourceFile)
import qualified Data.Conduit.List     as C
import           Numeric
import           System.Environment


main = do
    mode : rest <- getArgs
    case mode of
      "cards" -> test rest
      _ -> error "Please provide a mode for this test."


test [fp] = do
    mapM_ (testCard fp)
      [ 1000, 10000, 50000, 100000, 200000, 500000, 1000000, 3000000
      , 5000000, 10000000, 25000000]


testCard fp n = do
    let cnt = mempty :: HyperLogLog $(nat 14)
    v <- count cnt fp n
    print $ size v
    let c = size v
        len' = fromIntegral n
        est = fromIntegral $ c ^. estimate
    putStrLn $ unwords 
        [ "Test:", show n
        , "Est:", show c
        , "Err:", showGFloat (Just 2) (((est - len') / len') * 100) "", "%"]


-- | count uniques in a single col file
count :: ReifiesConfig n => HyperLogLog n -> FilePath -> Int -> IO (HyperLogLog n)
count cnt fp len = runResourceT $ do
    sourceFile fp $=
      intoCSV defCSVSettings $=
      C.isolate len $$
      C.fold step cnt
    where 
      step acc (x :: Row ByteString) = cons x acc
            


