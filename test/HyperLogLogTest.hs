{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Main where

-------------------------------------------------------------------------------
import Control.Lens
import Data.Approximate
import Data.Monoid
import Control.Lens.Cons
import           Data.ByteString.Char8 (ByteString)
import           Data.Conduit
import           Data.Conduit.Binary   (sourceFile)
import qualified Data.Conduit.List     as C
import           Data.CSV.Conduit
import           Numeric
import           System.Environment
import           Data.HyperLogLog
------------------------------------------------------------------------------- 


main = do
    mode : rest <- getArgs
    case mode of
      "cards" -> test rest
      _ -> error "Please provide a mode for this test."


test [fp] = do
    mapM_ (testCard fp)
      [ 1000, 2000, 3000, 4000, 5000, 5500, 7500, 10000, 20000, 30000, 50000, 100000, 200000, 500000, 1000000
      -- , 3000000
      -- , 5000000, 10000000, 25000000
      ]


testCard fp n = do
    (v :: HyperLogLog 14) <- count fp n
    let s = size v
        c = view estimate s
        len' = fromIntegral n
        c' = fromIntegral c
    putStrLn $ unwords [ "Test:", show n
                       , "Est:", show c
                       , "Err:", showGFloat (Just 2) (((c' - len') / len') * 100) "", "%"
                       , "Hi:", show (view hi s)
                       , "Lo:", show (view lo s)
                       ]




-- | count uniques in a single col file
count :: FilePath -> Int -> IO (HyperLogLog 14)
count fp len = runResourceT $ do
    sourceFile fp $=
      intoCSV defCSVSettings $=
      C.isolate len $$
      C.fold step mempty
    where
      step :: HyperLogLog 14 -> Row ByteString -> HyperLogLog 14
      step hll [x] = cons x hll


