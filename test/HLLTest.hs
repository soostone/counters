{-# LANGUAGE ScopedTypeVariables #-}

module Main where

import           Data.ByteString.Char8 (ByteString)
import           Data.Conduit
import           Data.Conduit.Binary   (sourceFile)
import qualified Data.Conduit.List     as C
import           Data.CSV.Conduit
import           Numeric
import           System.Environment

import           Counters.HyperLogLog


main = do
    mode : rest <- getArgs
    case mode of
      "cards" -> test rest
      "cast" -> testCast  rest
      _ -> error "Please provide a mode for this test."


test [fp] = do
    mapM_ (testCard fp)
      [ 1000, 10000, 50000, 100000, 200000, 500000, 1000000, 3000000
      , 5000000, 10000000, 25000000]


testCard fp n = do
    (v :: HLL D14) <- count fp n
    let c = cardinality v
        len' = fromIntegral n
        c' = fromIntegral c
    putStrLn $ unwords [ "Test:", show n
                       ,  "Est:", show c
                       , "Err:", showGFloat (Just 2) (((c' - len') / len') * 100) "", "%"]


testCast [fp, n] = do
    let n' = read n
    (v :: HLL D10) <- count fp n'
    let c = cardinality v
        v' = castHLL v :: HLL D9
        c' = cardinality v'
        v'' = castHLL v :: HLL D11
        c'' = cardinality v''
        v''' = castHLL v :: HLL D12
        c''' = cardinality v'''
    putStrLn $ unwords [ "Orig Card:", show n'
                       , "D10 Card:", show c
                       , "D9 Cast Card:", show c'
                       , "D11 Cast Card:", show c''
                       , "D12 Cast Card:", show c'''
                       ]



-- | count uniques in a single col file
count :: Nat n => FilePath -> Int -> IO (HLL n)
count fp len = runResourceT $ do
    sourceFile fp $=
      intoCSV defCSVSettings $=
      C.isolate len $$
      C.fold step initHLL
    where
      step :: HLL n -> Row ByteString -> HLL n
      step hll [x] = updateHLL hll x


